﻿Shader "Unlit/Phong"
{
    Properties
    {
		[Header(Diffuse)]
        _MainTex ("Texture", 2D) = "white" {}
		_Diffuse ("Diffuse Color", Color) = (1,1,1,1)
		_LightColorFactor ("Light Color Factor", Range(0,1)) = 0.5
		[Header(Ambient)]
		_AmbientLightColor ("Ambient Light Color", Color) = (1,1,1,1)
		_AmbientFactor ("Ambient Light Strength", Range(0,2)) = 0.5
		[Header(Specular)]
		_Shininess ("Shininess", Range(0.1,40)) = 1
		_SpecularColor ("Specular Color", Color) = (0.2,0.2,0.2,1)
		[Header(Fresnel)]
		_FresnelColor ("Fresnel Color", Color) = (0.2,0.2,0.2,1)
		_FresnelIntensity ("Fresnel Intensity", float) = 0.5
		_FPOW("Fresnel Power", float) = 3
		[Header(Shadows)]
		_ShadowIntensity("Shadow Intensity", Range(0,1)) = 0.6
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" "LightMode"="ForwardBase" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#pragma multi_compile_fwdbase

            #include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 pos : SV_POSITION; // has to be named pos if you want to cast and receive shadows
				float3 worldPos : TEXCOORD1;
				float3 worldNormal : TEXCOORD2;
				float3 normal : TEXCOORD3;
				SHADOW_COORDS(4) 
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float4 _AmbientLightColor;
			float4 _Diffuse;
			float _AmbientFactor;
			float _LightColorFactor;
			float _Shininess;
			float4 _SpecularColor;
			float4 _FresnelColor;
			float _FresnelIntensity;
			float _FPOW;
			float _ShadowIntensity;

            v2f vert (appdata_base v)
            {
                v2f o;
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex);
				o.worldNormal = normalize(mul(v.normal, (float3x3)unity_WorldToObject));
                o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.normal = v.normal;
				TRANSFER_SHADOW(o)
                return o;
            }

			fixed4 frag(v2f i) : SV_Target
			{
				float3 lightDir = normalize(_WorldSpaceLightPos0.xyz);
				float3 worldNormal = i.worldNormal;
				float3 worldNormal2 = normalize(mul(i.normal, (float3x3)unity_WorldToObject));
				float3 viewDir = normalize(_WorldSpaceCameraPos.xyz - i.worldPos.xxyz);

				float4 ambient = _AmbientLightColor * _AmbientFactor;

				float4 NDotL = saturate(dot(worldNormal, lightDir));
				float4 diffuse = _Diffuse * NDotL;
				diffuse = lerp(diffuse, _LightColor0 * diffuse, _LightColorFactor);

				float3 reflection = normalize(reflect(-lightDir, worldNormal));
				float RDotV = 0.5 * dot(reflection, viewDir) + 0.5;
				// alternative halfVector
				float3 halfVector = normalize(lightDir + viewDir);
				float NDotH = max(0, dot(worldNormal2, halfVector));
				_Shininess *= _Shininess;
				float4 specular = pow(RDotV, _Shininess) * _SpecularColor * _LightColor0;
				
				float shadow = SHADOW_ATTENUATION(i);

				float4 light = (diffuse + ambient) * lerp(1, shadow, _ShadowIntensity);
				float fresnel = _FresnelIntensity * (1 - (0.5 * dot(worldNormal, viewDir) + 0.5));
				fresnel = pow(fresnel, _FPOW);
				light = lerp(light, _FresnelColor, fresnel);

                fixed4 col = tex2D(_MainTex, i.uv) * light + specular;
                return col;
            }
            ENDCG
        }
		UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
    }
}
